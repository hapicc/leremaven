package com.mycompany.leremaven;

import clare.Clare;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.model.Musica;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/file")
public class UploadFileService {

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {

        String uploadedFileLocation = "C:\\Users\\Hanna\\Desktop\\teste\\" + fileDetail.getFileName();
        
        System.out.println( fileDetail.getFileName());
        writeToFile(uploadedInputStream, uploadedFileLocation);

        Clare c = new Clare();
        ArrayList<String> n = new ArrayList<>();
        try {
            n = c.clare(uploadedFileLocation, "C:\\Users\\Hanna\\Desktop\\", UUID.randomUUID().toString());
            if (n.size() > 1) {
                Musica musica = new Musica();
                musica.setNivel(Integer.valueOf(n.get(0)));
                musica.setCaminhomidi(n.get(2));
                musica.setCaminhoxml(n.get(1));

                ObjectMapper mapper = new ObjectMapper();
                mapper.writeValue(new File("C:\\Users\\Hanna\\Desktop\\teste\\file.json"), musica);
                String jsonInString = mapper.writeValueAsString(musica);

                return Response.status(200).entity(jsonInString).type(MediaType.APPLICATION_JSON).build();
            }
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(UploadFileService.class.getName()).log(Level.SEVERE, null, ex);
        }
        Musica musica = new Musica();
        musica.setCaminhomidi(n.get(0));
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = "";
        try {
            mapper.writeValue(new File("C:\\Users\\Hanna\\Desktop\\teste\\file.json"), musica);
            jsonInString = mapper.writeValueAsString(musica);
        } catch (IOException ex) {
            Logger.getLogger(UploadFileService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(500).entity(jsonInString).type(MediaType.APPLICATION_JSON).build();

    }

    private void writeToFile(InputStream uploadedInputStream,
            String uploadedFileLocation) {
        try {
            OutputStream out = new FileOutputStream(new File(
                    uploadedFileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

}
